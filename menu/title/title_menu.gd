extends Control

signal start_game()
signal show_settings()
signal show_credits()
signal quit_game()
signal update_tutorial()

var highest_score: MatchData = null

func _ready() -> void:
	update_tutorial.emit(%CheckBox.button_pressed)
	
	if highest_score == null:
		%HighScorePanel.hide()
	else:
		%Name.text = str(highest_score.name)
		%Score.text = str(highest_score.score)

func _unhandled_input(event):
	if event is InputEventMouseButton and\
			event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
		skip_animation()

func animation_start() -> void:
	%ButtonAnimation.play("show")

func skip_animation() -> void:
	if %ButtonAnimation.is_playing():
		%ButtonAnimation.seek(%ButtonAnimation.current_animation_length)

func _on_start_button_pressed() -> void:
	start_game.emit()

func _on_settings_button_pressed() -> void:
	show_settings.emit()

func _on_credits_button_pressed() -> void:
	show_credits.emit()

func _on_exit_button_pressed():
	quit_game.emit()

func _on_check_box_pressed() -> void:
	update_tutorial.emit(%CheckBox.button_pressed)
