extends Control

signal back()

func _on_back_button_pressed() -> void:
	back.emit()
