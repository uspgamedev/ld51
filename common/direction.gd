class_name Direction extends Object

const TOP: int = 0
const RIGHT: int = 1
const BOTTOM: int = 2
const LEFT: int = 3

static func rotate_left(dir: int) -> int:
	return (dir + 3) % 4

static func rotate_right(dir: int) -> int:
	return (dir + 1) % 4

static func to_vec2i(dir: int) -> Vector2i:
	match dir:
		Direction.TOP:
			return Vector2i.UP
		Direction.RIGHT:
			return Vector2i.RIGHT
		Direction.BOTTOM:
			return Vector2i.DOWN
		Direction.LEFT:
			return Vector2i.LEFT
	return Vector2i.UP
