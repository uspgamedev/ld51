class_name Locator
extends RefCounted

var scene_tree: SceneTree

func _init(tree_: SceneTree) -> void:
	scene_tree = tree_

func find_map() -> Map:
	return scene_tree.get_nodes_in_group(Groups.MAP)[0] as Map

func find_simulation() -> Simulation:
	return scene_tree.get_nodes_in_group(Groups.SIMULATION)[0] as Simulation

func find_placer() -> BuildingPlacer:
	return scene_tree.get_nodes_in_group(Groups.PLACER)[0] as BuildingPlacer
