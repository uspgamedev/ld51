class_name SignalHelper extends Object

static func safe_disconnect(signal_obj: Signal, callable: Callable):
	if signal_obj.is_connected(callable):
		signal_obj.disconnect(callable)
