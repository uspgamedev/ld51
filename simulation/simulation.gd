extends Node2D
class_name Simulation

signal pulse_emited()
signal matter_produced(amount)

@export var time_speed := 1.0
@export var pulse_clock := 10

@export var MAX_PRIORITY := 10

var buildings : Array[Building] = []
var tiles := {}
var effects_buckets := {}

var time := 0.0

func add_building(building: Building) -> void:
	assert(not tiles.has(building.tile))
	tiles[building.tile] = building
	buildings.append(building)
	building.tree_exited.connect(_on_building_destroyed.bind(building))

func has_building(building: Building) -> bool:
	return buildings.has(building)

func get_building(tile: Vector2i) -> Building:
	if tiles.has(tile):
		return tiles[tile]
	return null

func _on_pulsed():
	pulse_emited.emit()
	_propagate_pulse()
	_apply_effect()

func _on_building_destroyed(building: Building):
	tiles.erase(building.tile)
	buildings.remove_at(buildings.find(building))

func _propagate_pulse() -> void:
	effects_buckets.clear()
	for building in buildings:
		var effects := building.pulse(self)
		for effect in effects:
			if not effects_buckets.has(effect.get_priority()):
				effects_buckets[effect.get_priority()] = []
			effects_buckets[effect.get_priority()].append(effect)
		
func _apply_effect() -> void:
	for i in MAX_PRIORITY:
		if not effects_buckets.has(i):
			continue
		var bucket = effects_buckets[i]
		for effect in bucket:
			effect.apply(self)
