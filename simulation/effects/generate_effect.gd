extends PulseEffect
class_name GenerateEffect

var source : Building
var destination : Building
var generated_energy : int

func _init(_source: Building, _destination: Building, _generated_energy: int):
	source = _source
	destination = _destination
	generated_energy = _generated_energy

func apply(simulation : Simulation) -> void: 
	if not simulation.has_building(source) or not simulation.has_building(destination):
		return

	var dst = destination.get_component(Energizable) as Energizable

	dst.put_energy(generated_energy)
	if generated_energy > 0:
		destination.dispatch_call_all('energy_transferred_in', [source])

func get_priority() -> int:
	return 2
