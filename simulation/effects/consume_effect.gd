extends PulseEffect
class_name ConsumeEffect

var consumer: Building

func _init(_consumer: Building):
	consumer = _consumer

func apply(simulation: Simulation) -> void: 
	if not simulation.has_building(consumer):
		return 
	consumer.dispatch_call("consume")

func get_priority() -> int:
	return 3
