extends PulseEffect
class_name ChargeEffect

var generator: Building
var charge := 0.0

func _init(_generator: Building, _charge: float):
	generator = _generator
	charge = _charge

func apply(simulation : Simulation) -> void: 
	if not simulation.has_building(generator):
		return 
	generator.dispatch_call_all("charge")

func get_priority() -> int:
	return 2
