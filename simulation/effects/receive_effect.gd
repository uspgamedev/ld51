extends PulseEffect
class_name ReceiveEffect

var source : Building
var destination : Building
var transmitted_energy : int

func _init(_source: Building, _destination: Building, _transmitted_energy: int):
	source = _source
	destination = _destination
	transmitted_energy = _transmitted_energy

func apply(simulation : Simulation) -> void: 
	if not simulation.has_building(source) or not simulation.has_building(destination):
		return

	var src = source.get_component(Energizable) as Energizable
	var dst = destination.get_component(Energizable) as Energizable
	
	if dst == null:
		if src != null:
			src.total_energy += transmitted_energy
		return
	
	var residual_energy = dst.put_energy(transmitted_energy)
	if transmitted_energy > 0:
		destination.dispatch_call_all('energy_transferred_in', [source])
	if src != null:
		src.total_energy += residual_energy
	


func get_priority() -> int:
	return 2
