extends PulseEffect
class_name TransmitterEffect

var source : Building
var destination : Building
var transmitted_energy : int
var receive_effect : ReceiveEffect

func _init(_source: Building, _destination: Building, _transmitted_energy: int, _recieve_effect: ReceiveEffect):
	source = _source
	destination = _destination
	transmitted_energy = _transmitted_energy
	receive_effect = _recieve_effect

func apply(simulation : Simulation) -> void: 
	if not simulation.has_building(source) or not simulation.has_building(destination):
		return

	var src = source.get_component(Energizable) as Energizable
	var dst = destination.get_component(Energizable) as Energizable
	
	if dst == null:
		return

	var available_energy := src.give_energy(transmitted_energy)
	receive_effect.transmitted_energy = available_energy
	if transmitted_energy > 0:
		source.dispatch_call_all('energy_transferred_out', [destination])

func get_priority() -> int:
	return 1
