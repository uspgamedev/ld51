extends Sprite2D


@export var matter_producer_path: NodePath

@onready var delay: SceneTreeTimer

func _process(_delta):
	var matter_producer := get_node_or_null(matter_producer_path) as MatterProducer
	if matter_producer != null:
		if matter_producer.is_producing():
			show()
		elif delay == null:
			delay = get_tree().create_timer(2.0)
			delay.timeout.connect(_on_delay_timeout)

func _on_delay_timeout():
	hide()
	delay = null
