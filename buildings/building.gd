extends Node2D
class_name Building

@export
var building_spec: BuildingSpec

var tile : Vector2i

func pulse(simulation: Simulation) -> Array[PulseEffect]:
	var effects : Array[PulseEffect] = []
	for component in get_children():
		if component.has_method("pulse"):
			effects.append_array(component.pulse(simulation))
	return effects

func get_component(component: GDScript) -> Component:
	for child in get_children():
		if child.get_script() == component:
			return child
	return null

func dispatch_call(method: String, args : Array = []):
	for component in get_children():
		if component.has_method(method):
			return component.callv(method, args)
		
func dispatch_call_all(method: String, args : Array = []):
	for component in get_children():
		if component.has_method(method):
			component.callv(method, args)
	
