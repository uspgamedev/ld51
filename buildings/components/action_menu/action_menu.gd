extends Node2D

signal option_selected(building, action_index, action_name)

@export
var action_button_scene: PackedScene

@export
var padding := 8

func _ready() -> void:
	var locator := Locator.new(get_tree())
	var map := locator.find_map()
	$CanvasLayer/CloseArea.close_action_menu.connect(close_action_menu.bind(map, $VBoxContainer))
	
	for c in get_children():
		if c is ActionMenuAction:
			var new_button := action_button_scene.instantiate()
			new_button.text = c.action_name
			var menu := $VBoxContainer
			menu.add_child(new_button)
			new_button.connect("pressed", _on_action_selected.bind(c.action_index, c.action_name))

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		var menu := $VBoxContainer
		if menu.visible:
			var locator := Locator.new(get_tree())
			var map := locator.find_map()
			close_action_menu(map, menu)

func _on_area_2d_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_RIGHT and event.is_pressed():
			var menu := $VBoxContainer
			var locator := Locator.new(get_tree())
			var map := locator.find_map()
			if not menu.visible:
				if not map.disabled:
					open_action_menu(map, menu)
			else:
				close_action_menu(map, menu)

func _on_action_selected(action_index: int, action_name: String) -> void:
	var menu := $VBoxContainer
	var locator := Locator.new(get_tree())
	var map := locator.find_map()
	close_action_menu(map, menu)
	option_selected.emit(get_parent(), action_index, action_name)

func open_action_menu(map: Map, menu: VBoxContainer) -> void:
	map.disabled = true
	menu.show()
	menu.position.y = -menu.size.y / 2

func close_action_menu(map: Map, menu: VBoxContainer) -> void:
	map.disabled = false
	menu.hide()
