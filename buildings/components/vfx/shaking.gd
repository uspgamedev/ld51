class_name Shaking extends Component

@export var target_path: NodePath
@export var ticks := 80
@export var frequency := 60
@export var max_distance := 10.0

@onready var target := get_node_or_null(target_path) as Node2D

func _ready():
	for i in ticks:
		var distance: float = max_distance * (ticks - i) / ticks 
		var angle := randf() * TAU
		target.position = Vector2.RIGHT.rotated(angle) * distance
		await get_tree().create_timer(1.0 / frequency).timeout

