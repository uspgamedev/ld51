class_name Metamorphic extends Activable

@export var top: Texture
@export var right: Texture
@export var bottom: Texture
@export var left: Texture

var preview_top: Texture
var preview_right: Texture
var preview_bottom: Texture
var preview_left: Texture



func _activate():
	var body := building.get_component(Body) as Body
			
	preview_top = body.top
	preview_right = body.right
	preview_bottom = body.bottom
	preview_left = body.left
	
	body.top = top
	body.right = right
	body.bottom = bottom
	body.left = left
	body.refresh_sprite()
	
func _desactivate():
	var body := building.get_component(Body) as Body
	
	body.top = preview_top
	body.right = preview_right
	body.bottom = preview_bottom
	body.left = preview_left
	
	preview_top = null
	preview_right = null
	preview_bottom = null
	preview_left = null
	body.refresh_sprite()
	
