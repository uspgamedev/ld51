class_name Activable extends Component

@export var activated := false
@export var reactivable := false

func activate() -> void:
	if reactivable or not activated:
		_activate()
	activated = true

func desactivate() -> void:
	if reactivable or activated:
		_desactivate()
	activated = false
	
func _activate() -> void:
	return 
	
func _desactivate() -> void:
	pass
