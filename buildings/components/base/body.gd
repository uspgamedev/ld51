extends Component
class_name Body

@export
var top: Texture

@export
var right: Texture

@export
var bottom: Texture

@export 
var left: Texture

@export_enum("TOP", "RIGHT", "BOTTOM", "LEFT")
var direction := Direction.TOP :
	get:
		return direction
	set(val):
		direction = val
		refresh_sprite()

@export
var offset := Vector2.ZERO

func _ready() -> void:
	refresh_sprite()

func get_direction() -> Vector2i:
	return Direction.to_vec2i(direction)
	

func refresh_sprite() -> void:
	match direction:
			Direction.TOP:
				if top:
					$Sprite2d.texture = top
			Direction.RIGHT:
				if right:
					$Sprite2d.texture = right
			Direction.BOTTOM:
				if bottom:
					$Sprite2d.texture = bottom
			Direction.LEFT:
				if left:
					$Sprite2d.texture = left
			_:
				$Sprite2d.texture = top
	$Sprite2d.offset = offset
