extends Component
class_name Generator

@export var energy_rate := 1

func pulse(simulation: Simulation) -> Array[PulseEffect]:
	var effects: Array[PulseEffect] = []
	var directions = [Vector2i(-1, 0), Vector2i(0, 1), Vector2i(1, 0), Vector2i(0, -1)]
	for direction in directions:
		var destination := simulation.get_building(building.tile + direction)
		if destination == null or destination.get_component(Energizable) == null:
			continue
		var effect := GenerateEffect.new(building, destination, energy_rate)
		effects.append(effect)
	
	var effect := ChargeEffect.new(building, energy_rate)
	effects.append(effect)
	return effects
	
#func charge() -> void:
#	var energizable = building.get_component(Energizable) as Energizable
#	energizable.total_energy += energy_rate
