extends Component
class_name Energizable

signal vfx_added(vfx)

@export var limited_energy := true
@export var energy_limit := 1
@export var zap_scn: PackedScene
@export var activatables: Node #Array[Node]

var total_energy := 0 : set = _set_energy

func put_energy(energy: int) -> int:
	total_energy += energy
	var body = building.get_component(Body) as Body
	if energy > 0 and zap_scn != null and body != null:
		var zap := zap_scn.instantiate() as Node2D
		var front = Vector2(body.get_direction())
		zap.position = building.position #- front * 32.0
		zap.rotation = front.angle()
		vfx_added.emit(zap)
	return 0

func give_energy(required_energy: int) -> int:
	if total_energy < required_energy:
		var aux_energy = total_energy
		total_energy = 0
		return aux_energy
	total_energy -= required_energy
	return required_energy

func _set_energy(energy):
	total_energy = energy
	if limited_energy and total_energy > energy_limit:
		total_energy = energy_limit
		building.dispatch_call_all("take_damage", [1])
		
	if total_energy > 0:
#		for activatable in activatables:
		if activatables:
			activatables.activate()
	else:
#		for activatable in activatables:
		if activatables:
			activatables.desactivate()
	
		
#	var lightning = building.get_component(Lightning)
#	if lightning:
#		lightning.show(energy > 0)
	return energy
