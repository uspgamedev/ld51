extends Component
class_name Conductor

func pulse(simulation: Simulation) -> Array[PulseEffect]:
	var body = building.get_component(Body) as Body
	if body == null:
		return []
	var front = body.get_direction()
	var destination := simulation.get_building(building.tile + front)
	if destination == null:
		return []
		
	var energizable := building.get_component(Energizable) as Energizable
	if energizable.total_energy >= 0:
		var effects : Array[PulseEffect] = []
		var receive_effect := ReceiveEffect.new(building, destination, energizable.total_energy)
		var transmitter_effect := TransmitterEffect.new(building, destination, energizable.total_energy, receive_effect)
		effects.append(transmitter_effect)
		effects.append(receive_effect)
		return effects
	return []
	

