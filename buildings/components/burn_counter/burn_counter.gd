extends Control

@export var burn_icon_scn: PackedScene

func set_burn(current_value: int):
	while current_value > $HBoxContainer.get_child_count():
		var burn_icon := burn_icon_scn.instantiate()
		$HBoxContainer.add_child(burn_icon)
