class_name MatterProducer extends Component

signal matter_produced(amount)

@export var matter_production_rate := 20

var available_matter := 0
var matter_production := 0.0

func apply_consume():
	available_matter += matter_production_rate

func _process(delta):
	delta = delta * pulse_rate
	
	if available_matter > 0:
		matter_production += matter_production_rate * delta * 4
		if matter_production >= 1.0:
			if matter_production > available_matter:
				matter_production = available_matter
			matter_produced.emit(int(matter_production))
			available_matter -= int(matter_production)
			matter_production -= int(matter_production)
		
	
func is_producing() -> bool:
	return available_matter > 0

