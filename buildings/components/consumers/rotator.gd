class_name Rotator extends Component

var next_direction: int

func _ready():
	super()
	next_direction = building.get_component(Body).direction

func energy_transferred_in(source: Building):
	var simulation := Locator.new(get_tree()).find_simulation()
	var body := building.get_component(Body) as Body
	var front := body.direction
	var dir := Direction.rotate_right(front)
	while dir != front:
		var dir_offset := Direction.to_vec2i(dir) as Vector2i
		var destination := simulation.get_building(building.tile + dir_offset)
		if destination != null and destination != source:
			var energizable := destination.get_component(Energizable) as Energizable
			if energizable:
				break
		dir = Direction.rotate_right(dir)
	next_direction = dir

func energy_transferred_out(_destination: Building):
	var body := building.get_component(Body) as Body
	body.direction = next_direction
