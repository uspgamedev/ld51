extends Component
class_name Lighter

@export var texture : Texture2D
@export var life_rate := 0.5
@export var death_rate := 0.03
@export var life_ratio := 1.0 : set = _set_life_ratio
@export var overload := false
@export var max_overload := 0.0

@onready var height = $Light.texture.get_height()
	
func _ready():
	super._ready()
	if texture:
		$Light.texture = texture
	crop_light(life_ratio)


func _physics_process(delta):
	life_ratio -= death_rate * delta
	if life_ratio <= 0.0:
		building.dispatch_call_all("explode")
		life_ratio = 0.0

func _process(_delta):
	var siren := $SirenSFX as AudioStreamPlayer2D
	var danger := life_ratio / death_rate < 5.0
	if danger and not siren.playing:
		siren.play()
	elif not danger:
		siren.stop()

func apply_consume() -> void:
	life_ratio += life_rate
	

func _set_life_ratio(ratio: float) -> float:
	if ratio > 1.0:
		if overload and ratio > 1.0 + max_overload:
			building.dispatch_call_all("take_damage", [1])
			ratio = 1.0
		elif not overload:
			ratio = 1.0
	crop_light(ratio)
	life_ratio = ratio
	return ratio

func crop_light(ratio: float):
	if ratio < 0.0 or ratio > 1.0:
		return
	$Light.region_rect.position.y = int((1-ratio)*height)
	$Light.region_rect.size.y = int(ratio*height)
	$Light.offset.y = int(-ratio*height)
