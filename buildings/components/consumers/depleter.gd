extends Component
class_name Depleter

@export var max_uses := 10
var uses := 0
	
func pulse(_simulation: Simulation) -> Array[PulseEffect]:
	var effect := ConsumeEffect.new(building)
	return [effect]

func apply_consume() -> void:
	uses += 1
	%ProgressBar.value = 100.0 * (max_uses - uses) / max_uses
	if uses > max_uses:
		building.dispatch_call_all("explode")
		get_tree().get_nodes_in_group("Map")[0].change_tile(building.tile, Map.TILE_EMPTY)
	
