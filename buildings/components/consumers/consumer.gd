extends Component
class_name Consumer

@export var consume_rate := 1
	
func pulse(_simulation: Simulation) -> Array[PulseEffect]:
	var effect := ConsumeEffect.new(building)
	return [effect]

func consume() -> void:
	var energizable = building.get_component(Energizable)
	while  energizable.total_energy >= consume_rate:
		energizable.total_energy -= consume_rate
		building.dispatch_call_all("apply_consume")
