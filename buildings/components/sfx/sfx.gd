extends Node2D

@export_enum('Placed', 'Settled', 'Imploded', 'None')
var play_on_ready: int

func _ready():
	if play_on_ready < get_child_count():
		play_sfx(get_child(play_on_ready).name)

func play_sfx(child_name: String):
	var sfx := get_node_or_null(child_name) as AudioStreamPlayer2D
	if sfx != null:
		sfx.play()
