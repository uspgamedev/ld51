class_name Rotatable extends Component

func rotate_left():
	var body := building.get_component(Body) as Body
	if body == null:
		return
	body.direction = Direction.rotate_left(body.direction)

func rotate_right():
	var body := building.get_component(Body) as Body
	if body == null:
		return
	body.direction = Direction.rotate_right(body.direction)

func _on_action_menu_option_selected(_building, _action_index, action_name):
	if action_name == "Rotate Left":
		rotate_left()
	elif action_name == "Rotate Right":
		rotate_right()
