extends Node
class_name Component

var building: Building
var pulse_rate := 0.1 : 
	set(rate): pulse_rate = rate

func _ready():
	building = get_parent()

