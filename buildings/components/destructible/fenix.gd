class_name Fenix extends Component

@export var building_scene: PackedScene

func explode():
	var tile = building.tile
	var new_building := building_scene.instantiate()
	var map := Locator.new(get_tree()).find_map()
	var placer := Locator.new(get_tree()).find_placer()
	building.tree_exited.connect(placer.place_building.bind(map, tile, new_building))
