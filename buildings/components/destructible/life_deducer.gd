class_name LifeDeducer extends Component

signal life_lost

func explode():
	life_lost.emit()
