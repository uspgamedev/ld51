class_name Destructible extends Component

signal matter_produced(amount)
signal vfx_added(vfx)

@export var salvaged_matter := 10
@export var salvaged_vfx_scn: PackedScene

func _on_action_menu_option_selected(_building, _action_index, action_name: String):
	if action_name.match("*Delete*"):
		building.queue_free()
		matter_produced.emit(salvaged_matter)
		if salvaged_vfx_scn:
			var vfx := salvaged_vfx_scn.instantiate() as Node2D
			vfx.position = building.position
			vfx_added.emit(vfx)
