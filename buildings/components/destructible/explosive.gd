class_name Explosive extends Component

signal vfx_added(vfx)

@export var max_damage := 4

@export var explosion_scn: PackedScene

@onready var damage := 0

func take_damage(current_value: int):
	damage += current_value
	building.dispatch_call_all('set_burn', [damage])
	if damage >= max_damage:
		building.dispatch_call_all('explode')

func explode():
	building.queue_free()
	if explosion_scn != null:
		var explosion := explosion_scn.instantiate()
		explosion.position = building.position
		vfx_added.emit(explosion)
