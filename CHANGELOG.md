
# CHANGELOG

## 1.0.1

### Added

* Score counter
* Missing reference in credits

### Changed

* Hide high score if there is none
* Turn rotator into an actually useful building

### Fixed

* Fix transmitters not going dark when there is no energy in them
* Fix settlement 2 appearing with dislocated sprite
* Write settlement 2's in-game description

## 1.0.0

* Submission to Ludum Dare 51

