extends BuildingGenerator

@export var settlment_1 : PackedScene # : Array[PackedScene]
@export var settlment_2 : PackedScene

@export var upgrade_round := 6

var num_placed := 0

func generate() -> PackedScene:
	if num_placed % upgrade_round == upgrade_round - 1:
		num_placed += 1
		return settlment_2
	num_placed += 1
	return settlment_1
	
func get_color() -> Color:
	if (num_placed-1) % upgrade_round == upgrade_round - 1:
		return Color("00fffb")
	return Color("ff8f2a")

func get_duration() -> float:
	if (num_placed-1) % upgrade_round == upgrade_round - 1:
		return 60.0
	return 30.0
