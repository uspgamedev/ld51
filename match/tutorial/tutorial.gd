extends Node2D

signal next()

@export_multiline
var power_sources_tutorial_description: String = ""

@export_multiline
var matter_sources_tutorial_description: String = ""

@export_multiline
var incoming_buildings_tutorial_description: String = ""

@export_multiline
var final_tutorial_description: String = ""

@onready
var tutorial_section_label: Label = %TutorialSectionLabel

@onready 
var tutorial_description_label: Label = %TutorialDescriptionLabel

var power_source_pos: Vector2
var matter_source_pos: Vector2
var incoming_building_pos: Vector2

func _ready() -> void:
	$CanvasLayer/MarginContainer.modulate = Color(Color.WHITE, 0.0)
	await get_tree().create_timer(0.1).timeout
	get_tree().paused = true
	
	var tween := create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 1.0), 1.0)
	await tween.finished
	
	var locator := Locator.new(get_tree())
	var map := locator.find_map()
	
	power_source_pos = get_global_pos(map.tutorial_power_source_tile, map)
	matter_source_pos = get_global_pos(map.tutorial_matter_source_tile, map)
	incoming_building_pos = get_global_pos(map.tutorial_incoming_building_itle, map)
	
	$RectFocus.global_position = incoming_building_pos
	tutorial_section_label.text = "Incoming Settlements"
	tutorial_description_label.text = incoming_buildings_tutorial_description
	await next
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 0.0), 1.0)
	await tween.finished
	
	$RectFocus.global_position = power_source_pos
	tutorial_section_label.text = "Power Sources"
	tutorial_description_label.text = power_sources_tutorial_description
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 1.0), 1.0)
	await tween.finished
	
	await next
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 0.0), 1.0)
	await tween.finished
	
	$RectFocus.global_position = matter_source_pos
	tutorial_section_label.text = "Matter Sources"
	tutorial_description_label.text = matter_sources_tutorial_description
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 1.0), 1.0)
	await tween.finished
	
	await next
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 0.0), 1.0)
	await tween.finished
	
	$RectFocus.hide()
	tutorial_section_label.text = "Go Ahead!"
	tutorial_description_label.text = final_tutorial_description
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 1.0), 1.0)
	await tween.finished
	
	await next
	
	tween = create_tween()
	tween.tween_property($CanvasLayer/MarginContainer, "modulate", Color(Color.WHITE, 0.0), 1.0)
	await tween.finished
	
	get_tree().paused = false
	queue_free()

func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_accept"):
		next.emit()

func get_global_pos(tile: Vector2i, map: Map) -> Vector2:
	return map._tilemap.to_global(
		map._tilemap.map_to_local(tile)
	)
