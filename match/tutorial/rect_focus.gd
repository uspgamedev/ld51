extends Node2D

var size : Vector2 = Vector2(80, 80);
var pos : Vector2 = -size / 2;
var corner_radius : int = 10;

func _draw() -> void:
	var style_box : StyleBoxFlat = StyleBoxFlat.new()
	style_box.bg_color = Color(Color.BLACK, 0.0);
	style_box.set_corner_radius_all(corner_radius);
	style_box.set_border_width_all(5);
	style_box.set_border_color(Color.RED);
	draw_style_box(style_box, Rect2(pos, size));
