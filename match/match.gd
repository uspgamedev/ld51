class_name Match
extends Control

signal debug_matter_produced(amount)
signal match_exited
signal match_saved(match_data)
signal pause_game

var match_data: MatchData
var game_over: bool = false

func _ready():
	match_data = MatchData.new()
	
	$HUD/GameOver.match_exited.connect(_on_match_exited)
	$HUD/GameOver.match_saved.connect(_on_match_saved)
	$Simulation.pulse_emited.connect(_on_pulse_emited)
	
	var settlement_spawn_timer := $SettlementSpawnTimer as Timer
	var building_placer := $BuildingPlacer as BuildingPlacer
	var map := $Center/Map as Map
	settlement_spawn_timer.timeout.connect(
			building_placer._on_settlement_spawned.bind(map)
	)
	$BuildingPlacer._on_settlement_spawned(map)

func _process(_delta):
	var build_palette := %BuildPalette as BuildPalette
	var building_placer := %BuildingPlacer as BuildingPlacer
	var selected_spec := build_palette.get_selected_building_spec()
	if selected_spec != null:
		var map := %Map as Map
		var thumbnail := selected_spec.thumbnails[building_placer.placing_dir]
		map.show_building_preview(thumbnail)

func _input(event):
	if event.is_action_pressed("debug_produce_matter"):
		debug_matter_produced.emit(10)

func _on_pulse_emited():
	if !game_over:
		match_data.score += 1
		%PulseClock.update_score(match_data.score)

func _on_game_over():
	%GameOver.show()
	%GameOver.score = match_data.score
	$Center/Map.disabled = true
	%GameOverSFX.play()
	game_over = true

func _on_match_exited():
	match_exited.emit()

func _on_match_saved(saved_name: String):
	match_data.name = saved_name
	match_saved.emit(match_data)

func _on_pause_button_pressed() -> void:
	pause_game.emit()
