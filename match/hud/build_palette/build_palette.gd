class_name BuildPalette extends PanelContainer

@export
var building_specs: Array[BuildingSpec]

func _ready():
	var building_list := $MarginContainer/BuildingList as ItemList
	for building_spec in building_specs:
		var text = "%s (%s M)" % [building_spec.name, building_spec.material_cost]
		building_list.add_item(text, building_spec.thumbnails[0])

func get_selected_building_spec() -> BuildingSpec:
	var building_list := $MarginContainer/BuildingList as ItemList
	var selected_items := building_list.get_selected_items()
	if selected_items.is_empty():
		return null
	else:
		return building_specs[selected_items[0]]
