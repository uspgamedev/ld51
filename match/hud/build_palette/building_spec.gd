class_name BuildingSpec extends Resource

@export var thumbnails: Array[Texture]
@export var name: String = 'unknown'
@export_multiline var description: String = "this is a description"
@export_file var building_up_path: String
@export_file var building_right_path: String
@export_file var building_down_path: String
@export_file var building_left_path: String
@export var material_cost: int = 10
@export var requires_power_source := false
@export var requires_matter_source := false

func get_building_scn(dir := Direction.TOP) -> PackedScene:
	var path: String
	match dir:
		Direction.TOP:
			path = building_up_path
		Direction.RIGHT:
			path = building_right_path
		Direction.BOTTOM:
			path = building_down_path
		Direction.LEFT:
			path = building_left_path
	return load(path)

func is_valid_position(map: Map, tile: Vector2i) -> bool:
	return (
			not map.has_incoming_building_at(tile) and
			requires_power_source == map.is_power_source(tile) and
			requires_matter_source == map.is_matter_source(tile)
	)
