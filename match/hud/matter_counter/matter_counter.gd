class_name MatterCounter extends PanelContainer

@export
var initial_value := 10

@onready
var count := initial_value

@onready
var _label := $MarginContainer/HBoxContainer/Label as Label

func _ready():
	get_tree().node_added.connect(_on_matter_producer_spawned)

func _process(_delta):
	_label.text = "%d " % count

func _on_matter_produced(amount):
	count += amount

func _on_matter_producer_spawned(matter_producer: Node):
	if matter_producer.has_signal('matter_produced'):
		matter_producer.matter_produced.connect(_on_matter_produced)
