extends TextureProgressBar

signal pulsed

@export
var period := 10.0

@onready
var _timer := $Timer as Timer

func _ready():
	_timer.wait_time = period
	update_score(0)

func _process(_delta):
	value = max_value * (period - _timer.time_left) / period
	%ParticleAxis.rotation = TAU * value / max_value

func update_score(score: int):
	%ScoreLabel.text = str(score)

func _on_timer_timeout():
	pulsed.emit()
	$PulseSFX.play()
