extends CenterContainer

signal match_exited
signal match_saved(name)

var score: int
var game_saved: bool = false

func _process(_delta: float) -> void:
	%ScoreLabel.text = "Score: %d" % score
	%SaveButton.disabled = %NameEdit.text == "" || game_saved

func _on_back_to_title_button_pressed():
	match_exited.emit()

func _on_save_button_pressed() -> void:
	match_saved.emit(%NameEdit.text)
	game_saved = true
