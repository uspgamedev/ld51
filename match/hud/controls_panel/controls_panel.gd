extends MarginContainer

var tweening: bool = false

func _ready() -> void:
	modulate = Color(Color.WHITE, 0.0)
	await get_tree().create_timer(0.5).timeout
	var tween: Tween = create_tween()
	tweening = true
	tween.tween_property(self, "modulate", Color(Color.WHITE, 1.0), 1.0)
	await tween.finished
	tweening = false

func _input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.keycode == KEY_CTRL and event.is_pressed():
			if not tweening:
				var tween: Tween = create_tween()
				if visible:
					tweening = true
					tween.tween_property(self, "modulate", Color(Color.WHITE, 0.0), 1.0)
					await tween.finished
					hide()
					tweening = false
				else:
					tweening = true
					show()
					tween.tween_property(self, "modulate", Color(Color.WHITE, 1.0), 1.0)
					await tween.finished
					tweening = false
