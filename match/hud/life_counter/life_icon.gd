class_name LifeIcon extends TextureRect

func turn_off():
	(texture as AtlasTexture).region.position.x = 32

func turn_on():
	(texture as AtlasTexture).region.position.x = 0
