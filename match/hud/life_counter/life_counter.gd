extends PanelContainer

signal game_over

@export var initial_life := 3
@export var life_icon_scn: PackedScene

@onready
var current_life := initial_life

var _icons: Array[LifeIcon] = []

func _ready():
	for _i in initial_life:
		var icon := life_icon_scn.instantiate() as LifeIcon
		icon.turn_on()
		$HBoxContainer.add_child(icon)
		_icons.append(icon)
	get_tree().node_added.connect(func(node: Node):
		if node.has_signal('life_lost'):
			node.connect('life_lost', self._on_life_lost)
	)

func _process(_delta):
	for i in _icons.size():
		if i + 1 <= current_life:
			_icons[i].turn_on()
		else:
			_icons[i].turn_off()

func _on_life_lost():
	current_life = max(current_life - 1, 0)
	if current_life == 0:
		game_over.emit()
