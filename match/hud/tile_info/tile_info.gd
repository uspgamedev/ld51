extends PanelContainer

@export
var tiles_metadata: Array[TileMetadata]

@onready
var tile_name_label := $VBoxContainer/MarginContainer2/TileNameLabel

@onready
var tile_description_label := $VBoxContainer/MarginContainer/TileDescriptionLabel

@onready
var texture_rect := $VBoxContainer/MarginContainer3/TextureRect

func _ready() -> void:
	var locator := Locator.new(get_tree())
	var map := locator.find_map()
	map.current_tile_changed.connect(_on_current_tile_changed)

func _on_current_tile_changed(tile_index: int, tile: Vector2i):
	var locator := Locator.new(get_tree())
	var map := locator.find_map()
	if map.has_incoming_building_at(tile):
		tile_name_label.text = tiles_metadata[3].tile_name
		tile_description_label.text = tiles_metadata[3].tile_description
		texture_rect.texture = tiles_metadata[3].thumbnail
	else:
		var simulation := locator.find_simulation()
		var building := simulation.get_building(tile)
		if building != null:
			tile_name_label.text = building.building_spec.name
			tile_description_label.text = building.building_spec.description
			texture_rect.texture = building.building_spec.thumbnails[0]
		else:
			tile_name_label.text = tiles_metadata[tile_index].tile_name
			tile_description_label.text = tiles_metadata[tile_index].tile_description
			texture_rect.texture = tiles_metadata[tile_index].thumbnail
