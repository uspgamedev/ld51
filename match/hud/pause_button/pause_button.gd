extends Button

func _ready():
	var tween: Tween = create_tween()
	tween.tween_property(self, "modulate", Color(Color.WHITE, 1.0), .5)
	await tween.finished
	disabled = false


func _on_pause_button_pressed():
	$ClickSFX.play()


func _on_pause_button_mouse_entered():
	$HoverSFX.play()
