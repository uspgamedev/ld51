extends CenterContainer

signal match_exited()
signal game_exited()

var paused: bool = false



func pause():
	paused = true
	show()
	get_tree().paused = true

func unpause():
	paused = false
	hide()
	get_tree().paused = false

func _on_back_to_title_button_pressed() -> void:
	unpause()
	match_exited.emit()

func _on_quit_game_button_pressed() -> void:
	unpause()
	game_exited.emit()
