class_name BuildingPlacer extends Node

@export var settlement_spawn_margin := 2
@export var settlement_gap_to_power_source := 2
@export var settlement_generator: Node
@export var build_palette_path: NodePath
@export var simulation_path: NodePath
@export var matter_counter_path: NodePath

@onready
var simulation: Simulation = get_node_or_null(simulation_path)

@onready
var matter_counter: MatterCounter = get_node_or_null(matter_counter_path)

@onready
var placing_dir: int = Direction.TOP

func _ready():
	assert(simulation and matter_counter)
	add_to_group("Placer")

func place_building(map: Map, tile: Vector2i, building: Building):
	building.tile = tile
	map.build(building)
	simulation.add_building(building)

func _unhandled_input(event):
	if event.is_action_pressed("rotate_placement_left"):
		placing_dir = Direction.rotate_left(placing_dir)
	elif event.is_action_pressed("rotate_placement_right"):
		placing_dir = Direction.rotate_right(placing_dir)
	

func _on_building_placed(map: Map, tile: Vector2i):
	var build_palette := get_node_or_null(build_palette_path) as BuildPalette
	if build_palette != null and simulation.get_building(tile) == null:
		var building_spec := build_palette.get_selected_building_spec()
		if (
				building_spec != null and
				building_spec.material_cost <= matter_counter.count and
				building_spec.is_valid_position(map, tile)
		):
			var scn := building_spec.get_building_scn(placing_dir)
			var building := scn.instantiate()
			place_building(map, tile, building)
			matter_counter.count -= building_spec.material_cost

func _on_settlement_spawned(map: Map):
	var bounds := Rect2i()
	for building in simulation.buildings:
		bounds.position.x = min(bounds.position.x, building.tile.x)
		bounds.position.y = min(bounds.position.y, building.tile.y)
		bounds.end.x = max(bounds.end.x, building.tile.x)
		bounds.end.y = max(bounds.end.y, building.tile.y)
	bounds.position.x -= settlement_spawn_margin
	bounds.position.y -= settlement_spawn_margin
	bounds.end.x += settlement_spawn_margin
	bounds.end.y += settlement_spawn_margin
	var spawn_position: Vector2i
	while true:
		spawn_position = Vector2i(
				randi_range(bounds.position.x, bounds.end.x),
				randi_range(bounds.position.y, bounds.end.y),
		)
		if (
				simulation.get_building(spawn_position) == null and
				not map.has_incoming_building_at(spawn_position) and
				not _has_power_source_nearby(map, spawn_position) and
				not map.is_matter_source(spawn_position)
		):
			break
	var settlement_scn = settlement_generator.generate()
	if settlement_scn != null:
		await map.add_incoming_building(spawn_position, settlement_generator.get_duration(), 
				settlement_generator.get_color())
		var settlement := settlement_scn.instantiate() as Building
		place_building(map, spawn_position, settlement)

func _has_power_source_nearby(map: Map, tile: Vector2i) -> bool:
	var begin = tile - Vector2i.ONE * settlement_gap_to_power_source
	var end = tile + Vector2i.ONE * settlement_gap_to_power_source
	for x in range(begin.x, end.x):
		for y in range(begin.y, end.y):
			var nearby_tile := Vector2i(x, y)
			if map.is_power_source(nearby_tile):
				return true
	return false
