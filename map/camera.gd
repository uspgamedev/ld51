class_name MapCamera extends Camera2D

@export
var smooth_factor := 1.0

@export
var sensitivity := 1.0

@onready
var zoom_sensitivity := 0.8

@onready
var zoom_min := 0.1

@onready
var zoom_max := 1.8

@onready
var _is_dragging := false

@onready
var _target_pos := Vector2.ZERO

var _drag_screen_origin: Vector2
var _drag_local_origin: Vector2

func _process(delta):
	if _is_dragging:
		var mouse_pos := get_global_mouse_position()
		var drag_offset := mouse_pos - _drag_screen_origin
		_target_pos = _drag_local_origin - drag_offset * sensitivity
	position += (_target_pos - position) * smooth_factor * delta
	if Input.is_action_pressed("zoom_in"):
		
		zoom += zoom_sensitivity * delta * Vector2(1, 1)
		if zoom.x > zoom_max:
			zoom = zoom_max * Vector2(1, 1)
	if Input.is_action_pressed("zoom_out"):
		zoom -= zoom_sensitivity * delta * Vector2(1, 1)
		if zoom.x < zoom_min:
			zoom = zoom_min * Vector2(1, 1)
	

func start_dragging():
	if not _is_dragging:
		_drag_screen_origin = get_global_mouse_position()
		_drag_local_origin = position
		_is_dragging = true

func stop_dragging():
	_is_dragging = false
