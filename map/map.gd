class_name Map extends Node2D

signal tile_clicked(map, tile)
signal current_tile_changed(tile_index, tile)

const TILE_EMPTY = 0
const TILE_POWER_SOURCE = 1
const TILE_MATTER_SOURCE = 2

@export
var disabled := false :
	get:
		return disabled
	set(val):
		disabled = val
		$TileSelector.visible = not disabled

@export var generation_size := 100
@export var chunk_size := 10
@export var power_sources_per_chunk := 2
@export var matter_sources_per_chunk := 2
@export var incoming_building_scn: PackedScene

@onready
var _tilemap := $TileMap as TileMap

@onready
var _camera := $Camera2d as MapCamera

@onready
var _incoming_building_tiles := {}

@onready var _tile_selector := $TileSelector as TileSelector

var tutorial_power_source_tile: Vector2i
var tutorial_matter_source_tile: Vector2i
var tutorial_incoming_building_itle: Vector2i

func _ready():
	$TileSelector.tile_size = _tilemap.tile_set.tile_size
	for x in range(-generation_size, generation_size):
		for y in range(-generation_size, generation_size):
			var tile := Vector2i(x, y)
			_tilemap.set_cell(0, tile, TILE_EMPTY, Vector2i.ZERO, 0)
	var chunk_origin := Vector2i(-generation_size, -generation_size)
	while true:
		var chunk := Rect2i(chunk_origin, Vector2i.ONE * chunk_size)
		for i in power_sources_per_chunk:
			var tile := _pick_random_tile_in_chunk(chunk)
			_tilemap.set_cell(0, tile, TILE_POWER_SOURCE, Vector2i.ZERO, 0)
		for i in matter_sources_per_chunk:
			var tile := _pick_random_tile_in_chunk(chunk)
			_tilemap.set_cell(0, tile, TILE_MATTER_SOURCE, Vector2i.ZERO, 0)
		chunk_origin.x += chunk_size
		if chunk_origin.x > generation_size:
			chunk_origin.x = -generation_size
			chunk_origin.y += chunk_size
		if chunk_origin.y > generation_size:
			break
	_tilemap.set_cell(0, Vector2i.ZERO, TILE_POWER_SOURCE, Vector2i.ZERO, 0)
	_tilemap.set_cell(0, Vector2i(3, 4), TILE_MATTER_SOURCE, Vector2i.ZERO, 0)
	tutorial_power_source_tile = Vector2i.ZERO
	tutorial_matter_source_tile = Vector2i(3, 4)
	get_tree().node_added.connect(func(node):
		if node.has_signal('vfx_added'):
			node.connect('vfx_added', self._on_vfx_addded)
	)

func _process(_delta):
	if not disabled:
		$TileSelector.show()
		var mouse_pos := get_local_mouse_position()
		$TileSelector.current_tile = _tilemap.local_to_map(mouse_pos)
		current_tile_changed.emit(
			_tilemap.get_cell_source_id(0, $TileSelector.current_tile),
			$TileSelector.current_tile
		)
	else:
		$TileSelector.hide()

func _unhandled_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.is_pressed():
			tile_clicked.emit(self, $TileSelector.current_tile)
		elif event.button_index == MOUSE_BUTTON_MIDDLE:
			if event.is_pressed():
				_camera.start_dragging()
			else:
				_camera.stop_dragging()

func build(building: Building):
	var local_pos := _tilemap.map_to_local(building.tile)
	building.position = local_pos
	$Buildings.add_child(building)

func add_incoming_building(tile: Vector2i, duration: float, color: Color):
	tutorial_incoming_building_itle = tile
	var incoming_building = incoming_building_scn.instantiate()
	incoming_building.get_node("Timer").wait_time = duration
	incoming_building.get_node("Arrow").color = color
	var local_pos := _tilemap.map_to_local(tile)
	incoming_building.position = local_pos
	$Buildings.add_child(incoming_building)
	_incoming_building_tiles[tile] = true
	await incoming_building.spawned
	_incoming_building_tiles.erase(tile)

func is_power_source(tile: Vector2i) -> bool:
	return _tilemap.get_cell_source_id(0, tile) == TILE_POWER_SOURCE

func is_matter_source(tile: Vector2i) -> bool:
	return _tilemap.get_cell_source_id(0, tile) == TILE_MATTER_SOURCE

func has_incoming_building_at(tile: Vector2i) -> bool:
	return _incoming_building_tiles.get(tile, false)

func show_building_preview(texture: Texture2D):
	_tile_selector.show_preview(texture)

func hide_building_preview():
	_tile_selector.hide()

func _pick_random_tile_in_chunk(chunk: Rect2i) -> Vector2i:
	return Vector2i(
			randi_range(chunk.position.x, chunk.end.x - 1),
			randi_range(chunk.position.y, chunk.end.y - 1),
	)

func _on_vfx_addded(vfx: Node2D):
	$VFXs.add_child(vfx)

func change_tile(tile: Vector2i, tile_index: int):
	_tilemap.set_cell(0, tile, tile_index, Vector2i.ZERO, 0)
	current_tile_changed.emit(_tilemap.get_cell_source_id(0, tile), tile)
