class_name TileSelector extends Node2D

var tile_size: Vector2
var current_tile: Vector2i

func _process(_delta):
	position = tile_size * (Vector2(current_tile) + Vector2.ONE / 2)

func show_preview(texture: Texture2D):
	var preview := $Preview as Sprite2D
	preview.texture = texture
	preview.offset.y = -(texture.get_size().y - tile_size.y) / 2
	preview.show()

func hide_preview():
	var preview := $Preview as Sprite2D
	preview.hide()
