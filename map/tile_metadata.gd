class_name TileMetadata
extends Resource

@export
var thumbnail: Texture

@export
var tile_name: String = ""

@export_multiline
var tile_description: String = ""
