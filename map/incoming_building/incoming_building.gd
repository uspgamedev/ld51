class_name IncomingBuilding extends Node2D

signal spawned

@onready
var _timer = %Timer as Timer

@onready
var _progress_bar = %ProgressBar as ProgressBar

func _process(_delta):
	var progress: float = (_timer.wait_time - _timer.time_left) / _timer.wait_time
	_progress_bar.value = progress * 100

func _on_timer_timeout():
	spawned.emit()
	queue_free()
