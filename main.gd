extends Control

const SAVED_DATA_PATH := "user://saved_data.tres"

@export
var splash_scn: PackedScene

@export
var title_scn: PackedScene

@export
var match_scene: PackedScene

@export
var tutorial_scene: PackedScene

@export
var settings_scene: PackedScene

@export
var credits_scene: PackedScene

@export
var play_tutorial: bool = true

@onready
var transition_manager: TransitionManager = $TransitionManager

var current_child: Node = null
var saved_data: SavedData
var highest_score: MatchData


func _ready():
	%PauseMenu.match_exited.connect(show_title)
	%PauseMenu.game_exited.connect(quit_game)
	
	load_save()
	
	show_splash()

func _input(event: InputEvent) -> void:
	if current_child is Match:
		if event.is_action_pressed("ui_cancel"):
			if not %PauseMenu.paused:
				%PauseMenu.pause()
			else:
				%PauseMenu.unpause()
	if event.is_action_pressed("toggle_fullscreen"):
		if get_tree().root.mode == Window.MODE_FULLSCREEN:
			get_tree().root.mode = Window.MODE_WINDOWED
		else:
			get_tree().root.mode = Window.MODE_FULLSCREEN

func show_splash():
	var splash := splash_scn.instantiate()
	add_child(splash)
	current_child = splash
	transition_manager.end_transition()
	await transition_manager.transition_finished
	splash.begin()
	await splash.finished
	show_title()

func show_title():
	load_save()
	transition_manager.begin_transition()
	await transition_manager.screen_dimmed
	if current_child != null:
		current_child.queue_free()
	var title := title_scn.instantiate()
	transition_manager.transition_finished.connect(title.animation_start)
	title.highest_score = highest_score
	add_child(title)
	current_child = title
	title.start_game.connect(start_game)
	title.show_settings.connect(show_settings)
	title.show_credits.connect(show_credits)
	title.quit_game.connect(quit_game)
	title.update_tutorial.connect(update_tutorial)
	transition_manager.end_transition()

func update_tutorial(val: bool):
	play_tutorial = val

func start_game():
	transition_manager.begin_transition()
	await transition_manager.screen_dimmed
	if current_child != null:
		current_child.queue_free()
	var new_match: Match = match_scene.instantiate()
	add_child(new_match)
	current_child = new_match
	new_match.match_exited.connect(show_title)
	new_match.pause_game.connect(%PauseMenu.pause)
	new_match.match_saved.connect(save_game)
	transition_manager.end_transition()
	
	if play_tutorial:
		var tutorial := tutorial_scene.instantiate()
		self.add_child(tutorial)

func load_save():
	if not FileAccess.file_exists(SAVED_DATA_PATH):
		saved_data = SavedData.new()
	else:
		saved_data = load(SAVED_DATA_PATH)
	
	for match_data in saved_data.data:
		if highest_score == null:
			highest_score = match_data
		elif match_data.score > highest_score.score:
			highest_score = match_data

func save_game(match_data: MatchData):
	saved_data.data.append(match_data)
	ResourceSaver.save(saved_data, SAVED_DATA_PATH)

func show_settings():
	transition_manager.begin_transition()
	await transition_manager.screen_dimmed
	if current_child != null:
		current_child.queue_free()
	var settings := settings_scene.instantiate()
	add_child(settings)
	current_child = settings
	settings.back.connect(show_title)
	transition_manager.end_transition()

func show_credits():
	transition_manager.begin_transition()
	await transition_manager.screen_dimmed
	if current_child != null:
		current_child.queue_free()
	var credits := credits_scene.instantiate()
	add_child(credits)
	current_child = credits
	credits.finished.connect(show_title)
	transition_manager.end_transition()

func quit_game():
	transition_manager.begin_transition()
	await transition_manager.screen_dimmed
	get_tree().quit()
