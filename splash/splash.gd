extends Control

signal finished

@export
var skip_actions: Array[String]

@export
var sustain_duration := 1.5

var _current_timer: SceneTreeTimer

func _on_sustain_finished():
	finished.emit()

func _input(event):
	for skip_action in skip_actions:
		if event.is_action_pressed(skip_action):
			clean_up()
			finished.emit()

func begin():
	_current_timer = get_tree().create_timer(sustain_duration)
	_current_timer.timeout.connect(_on_sustain_finished)

func clean_up():
	if _current_timer != null:
		SignalHelper.safe_disconnect(_current_timer.timeout, _on_sustain_finished)
