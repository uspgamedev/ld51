class_name TransitionManager
extends CanvasLayer

signal screen_dimmed()
signal transition_finished()

@export
var transition_duration: float = 0.7

@export
var transition_delay: float = 0.2

@export var faded_color: Color = Color.BLACK

func begin_transition() -> void:
	get_tree().call_group("button", "set_disabled", true)
	
	$ColorRect.color = Color(faded_color, 0.0)
	
	var tween := create_tween()
	tween.set_ease(Tween.EASE_OUT)
	tween.set_trans(Tween.TRANS_LINEAR)
	tween.tween_property($ColorRect, "color", Color(faded_color, 1.0), transition_duration)
	
	await tween.finished
	screen_dimmed.emit()

func end_transition() -> void:
	$ColorRect.color = Color(faded_color, 1.0)
	
	var tween := create_tween()
	tween.set_ease(Tween.EASE_OUT)
	tween.set_trans(Tween.TRANS_LINEAR)
	tween.tween_property($ColorRect, "color", Color(faded_color, 0.0), transition_duration)
	
	await tween.finished
	transition_finished.emit()
	
	get_tree().call_group("button", "set_disabled", false)
